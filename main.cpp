//Libreria de Mbed
#include "mbed.h"

//Direccion del Esclavo
#define     MPU6050_address             0xD0 //Direccion de 7 bit I2C de la MPU 6050

//Escalas (Giroscopio)
#define     GYRO_FULL_SCALE_250_DPS     0x00
#define     GYRO_FULL_SCALE_500_DPS     0x08
#define     GYRO_FULL_SCALE_1000_DPS    0x10
#define     GYRO_FULL_SCALE_2000_DPS    0x18

//Escalas (Acelerometro)
#define     ACC_FULL_SCALE_2_G          0x00
#define     ACC_FULL_SCALE_4_G          0x08
#define     ACC_FULL_SCALE_8_G          0x10
#define     ACC_FULL_SCALE_16_G         0x18

//Escalas de conversion especificas de la documentacion de la MPU6050 segun la escalizacion de 16 bits
#define SENSITIVITY_ACCEL       2.0/32768.0             //Valor de conversion del acelerometro
#define SENSITIVITY_GYRO        250.0/32768.0           //Valor de conversion del giroscopio
#define SENSITIVITY_TEMP        333.87                  //Valor de sensibilidad de temperatura
#define TEMP_OFFSET             21                      //Valor de compensacion de temperatura
#define SENSITIVITY_MAGN        (10.0*4800.0)/32768.0   //Valor de conversion del magnetometro

//Decalaracion de variables

//Valores "RAW" de tipo entero
int16_t raw_accelx, raw_accely, raw_accelz;
int16_t raw_gyrox, raw_gyroy, raw_gyroz;
int16_t raw_temp;

//Valores escalizados
float accelx, accely, accelz;
float gyrox, gyroy, gyroz;
float temp;

//Bytes
char cmd[2];
char data[1];
char GirAcel[14];

float buffer[500][8];
int i;
Timer t; // Definicion del objeto temporizador
float timer=0;

//Definicion del objeto de comunicacion serial
Serial pc (USBTX, USBRX);
//Definicion del objeto I2C
I2C i2c (PB_9, PB_8);//SDA, SCL

//Programa principal donde va a declarar todas las acciones previas a ejecutar en el programa
int main()
{
    //Desactiva el modo de hibernacion de la MPU6050
    cmd[0] = 0x6B;
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);

    pc.printf("PRUEBA DE CONEXION DEL GIROSCOPIO Y ACELEROMETRO \n\r");
    //
    //¿Quién soy yo para el mpu6050?
    //
    pc.printf("1. prueba de conexion de MPU6050... \n\r");//Verificacion de la conexion
    cmd[0] = 0x75;
    i2c.write(MPU6050_address, cmd, 1);
    i2c.read(MPU6050_address, data, 1);
    if (data[0] !=0x68) {   //Valor por defecto de registro
        pc.printf("Error de conexion con la MPU6050 \n\r");
        pc.printf("Ups, no soy la MPU6050, ¿quien soy? :S. Soy: %#x \n\r",data[0]);
        pc.printf("\n\r");
        while (1);
        //Si no hay error la conexion es exitosa
    } else {
        pc.printf("Conexion exitosa con la MPU6050 \n\r");
        pc.printf("Hola, ¿todo bien?... soy la MPU6050 XD \n\r");
        pc.printf("\n\r");
    }
    wait(0.1);
    //Configuracion del giroscopio
    cmd[0] = 0x1B;  //GYRO_CONFIG 0x1B
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    //Configuracion del acelerometro
    cmd[0] = 0x1C;  //ACCEL_CONFIG 0x1C
    cmd[1] = 0x00;
    i2c.write(MPU6500_address, cmd, 2);
    wait (0.01);
    //Ciclo infinito del programa
    while(1) {
        //Construccion de la aquisicion de datos
        if(pc.getc() == 'A') {
            //Ciclo "for" para la aquisicion de datos
            for(i=0; i<=499; i++) {
                cmd[0]=0x3B;
                i2c.write(MPU6050_address, cmd, 1);     //Escritura del registro de inicio
                i2c.read(MPU6050_address, GirAcel, 14); //Lectura en rafaga de los valores de la MPU
                t.reset();
                t.start();
                raw_accelx = GirAcel[0]<<8 | GirAcel[1];
                raw_accely = GirAcel[2]<<8 | GirAcel[3];
                raw_accelz = GirAcel[4]<<8 | GirAcel[5];
                raw_temp = GirAcel[6]<<8 |  GirAcel[7];
                raw_gyrox = GirAcel[8]<<8 | GirAcel[9];
                raw_gyroy = GirAcel[10]<<8 | GirAcel[11];
                raw_gyrox = GirAcel[12]<<8 | GirAcel[13];

                //Valores "RAW" escalizados a valor real
                accelx = raw_accelx*SENSITIVITY_ACCEL;
                accely = raw_accely*SENSITIVITY_ACCEL;
                accelz = raw_accelz*SENSITIVITY_ACCEL;
                gyrox = raw_gyrox*SENSITIVITY_GYRO;
                gyroy = raw_gyroy*SENSITIVITY_GYRO;
                gyroz = raw_gyroz*SENSITIVITY_GYRO;
                temp = (raw_temp/SENSITIVITY_TEMP)+21;
                wait_ms(9);
                wait_us(962);
                t.stop();
                timer = t.read ();
                //Imprimir valores
                pc.printf("El tiempo de %f segundos \r", timer);
                pc.printf("%d %.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f \n\r",i+1,accelx, accely, accelz, gyrox, gyroy, gyroz, temp);
            }
        }
    }
}